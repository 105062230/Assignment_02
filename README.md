# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report

### game process
* menu (可選擇前往game或是leader board)
* game (結束後前往record)
* record(input,可按按鍵選擇是否要record,並按下按鍵選擇replay,或是返回menu)
* leader board(按下按鍵返回menu)

### basic rule
* 方向鍵的下左右控制移動
* 採到到nails或是碰到天花板會減1次生命
* 掉出畫面或是生命為零時會結束遊戲

### correct physical properties
* 階梯會往上移動
* 若踏出階梯外會落下,採在階梯上則跟階梯(假階梯除外)一起移動
* 踏在傳送帶階梯上會水平位移
* 踏上彈簧階梯會彈跳
* 踏上假階梯會下滑

### 2 different kinds of platform
* 一般階梯
* nails
* 傳送帶階梯
* 彈簧階梯
* 假階梯

### additional sound effects
* menu bgm
* playing bgm
* leader board bgm
* 碰到nails或天花板音效
* 踏上彈簧階梯音效
* 踏上假階梯音效
* 拿到星星音效
* 拿到愛心音效
* 遊戲結束音效
* 按鍵音效

### leaderboard
* 會依據分數高低排名
* 列出前十名
* 若該名次無人,則該欄位空白

### features
* 拿到星星會加5分
* 拿到愛心會增加1次生命,上限15
* 用滑鼠按下pause會暫停,在按下螢幕任一處會重新開始