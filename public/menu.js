var menuState = {
    preload: function() {
        
        game.load.image('menubg', 'asset/menubg.jpg');
        game.load.image('p', 'asset/p.png');
        game.load.image('l', 'asset/l.png');
        game.load.audio('buttonMusic', 'asset/button.wav');
        game.load.audio('menubgmMusic', 'asset/menubgm.ogg');

    },
    create: function() {
    // Explain how to start the game
    this.buttonMusic = game.add.audio('buttonMusic');
    this.buttonMusic.loop = false;
    
    menubg = game.add.sprite(0, 0, 'menubg');
    pkey = game.add.sprite(160, 270, 'p');
    lkey = game.add.sprite(160, 340, 'l');
    this.menubgmMusic = game.add.audio('menubgmMusic');
    this.buttonMusic.play();
    this.menubgmMusic.loop = true;
    this.menubgmMusic.play();

        var title1Label = game.add.text(260, -50,
        '小朋友下樓梯', { font: '30px Arial', fill: '#ffffff' });
        title1Label.anchor.setTo(0.5, 0.5);
        game.add.tween(title1Label).to({y: 160},1000).easing(Phaser.Easing.Bounce.Out).start();

        var startLabel = game.add.text(250, 290,
        'play', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        
        var boardLabel = game.add.text(300, 365,
        'leader board', { font: '25px Arial', fill: '#ffffff' });
        boardLabel.anchor.setTo(0.5, 0.5);

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.P);
        upKey.onDown.add(this.start, this);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.L);
        downKey.onDown.add(this.board, this);
    },
    start: function() {
    // Start the actual game
        this.buttonMusic.play();
        this.menubgmMusic.stop();
        game.state.start('play');
    },
    board: function() {
        this.buttonMusic.play();
        this.menubgmMusic.stop();
        game.state.start('board');
    }
}; 