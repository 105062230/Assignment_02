var r = 0;
var person;

var inputState = {
    preload: function () {
    // Add a 'loading...' label on the screen
    game.load.image('recordbg', 'asset/recordbg.jpg');
    
    var name = '';
    var bmd;
    
    game.load.audio('buttonMusic', 'asset/button.wav');
    game.load.image('w', 'asset/w.png');
    game.load.image('enter', 'asset/enter.png');
    game.load.image('m', 'asset/m.png');

    

    },

    // Load all game assets
    
    create: function () {
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'm': Phaser.Keyboard.M
        });

        this.buttonMusic = game.add.audio('buttonMusic');
        this.buttonMusic.loop = false;

        recordbg = game.add.image(0, 0, 'recordbg');
        wkey =  game.add.sprite(120, 160, 'w');
        enterkey = game.add.sprite(120, 230, 'enter');
        mkey = game.add.sprite(120, 300, 'm');

        var inputLabel = game.add.text(game.width/2, 90,
            'your score : ' + distance, { font: '32px Arial', fill: '#ffffff' });
        inputLabel.anchor.setTo(0.5, 0.5);


        var recordLabel = game.add.text(230, 180,
            'record', { font: '28px Arial', fill: '#ffffff' });
        recordLabel.anchor.setTo(0.5, 0.5);
        var choiceLabel = game.add.text(230, 250,
            'replay', { font: '28px Arial', fill: '#ffffff' });
        choiceLabel.anchor.setTo(0.5, 0.5);
        var choiceLabel2 = game.add.text(230, 320,
            'menu', { font: '28px Arial', fill: '#ffffff' });
        choiceLabel2.anchor.setTo(0.5, 0.5);
    },

    update: function() {
        if(keyboard.enter.isDown) {
            this.buttonMusic.play();
            this.replay();
        }
        if(keyboard.m.isDown) {
            this.buttonMusic.play();
            this.menu();
        }
        if(keyboard.w.isDown){
            this.buttonMusic.play();
            if(r==0)record();
            else alert('Your score is already upload.')
        }
    },

    menu: function(){
        this.post();
        distance = 0;
        r = 0;
        game.state.start('menu');
        status = 'running';
    },

    replay: function(){
        this.post();
        distance = 0;
        r = 0;
        game.state.start('play');
        status = 'running';
    },

    post: function(){
        if(r==1){
            var inputRef = firebase.database().ref('record');
            newpost = inputRef.push();
            newpost.set({
                    name: person,
                    score: distance
            }).catch(e => console.log(e.message));
        } 
    }
}; 

function record(){
        person = prompt("Please enter your name", "Harry Potter");
        if (person != '' && person!=null) {
            alert(person);
            r = 1;
        }
        else r = 0;
}