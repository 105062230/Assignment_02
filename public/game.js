// Initialize Phaser
var game = new Phaser.Game(400, 500, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0 };
// Add all the states
game.state.add('menu', menuState);
game.state.add('board', boardState);
game.state.add('play', playState);
game.state.add('input', inputState);
// Start the 'boot' state
game.state.start('menu');