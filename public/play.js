var lastTime = 0;
var player;
var keyboard;

var platforms = [];
var stars = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var text3;

var distance = 0;
var status = 'running';

var playState = {
    preload: function() {
        game.load.spritesheet('player', 'asset/player.png',32,32);
        game.load.image('playbg', 'asset/playbg.png');
        game.load.image('star', 'asset/star.png');
        game.load.image('heart', 'asset/heart.png');
        game.load.image('blood', 'asset/blood.png');
        game.load.image('ceiling', 'asset/ceiling.png');
        game.load.image('nails', 'asset/nails.png');
        game.load.image('normal', 'asset/normal.png');
        game.load.image('pixel', 'asset/pixel.png');
        game.load.image('wall', 'asset/wall.png');
        game.load.spritesheet('conveyorRight', 'asset/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'asset/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'asset/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'asset/fake.png', 96, 36);

        game.load.audio('bgmMusic', 'asset/playbgm.ogg');
        game.load.audio('bounceMusic', 'asset/bounce.wav');
        game.load.audio('nailMusic', 'asset/nail.wav');
        game.load.audio('fakeMusic', 'asset/fake.wav');
        game.load.audio('ggMusic', 'asset/gg.wav');
        game.load.audio('starMusic', 'asset/starsound.wav');
        game.load.audio('heartMusic', 'asset/heartsound.wav');
        
    }, // The proload state does nothing now.
    create: function() {
        // Removed background color, physics system, and roundPixels.
        // replace 'var score = 0' by global score variable.
        game.global.score = 0;
        // The following part is the same as in previous lecture.
        this.walls = game.add.group();
        
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });

        this.bgmMusic = game.add.audio('bgmMusic');
        this.bgmMusic.loop = true;
        this.bgmMusic.play();

        this.bounceMusic = game.add.audio('bounceMusic');
        this.bounceMusic.loop = false;
        this.nailMusic = game.add.audio('nailMusic');
        this.nailMusic.loop = false;
        this.ggMusic = game.add.audio('ggMusic');
        this.ggMusic.loop = false;
        this.starMusic = game.add.audio('starMusic');
        this.starMusic.loop = false;
        this.heartMusic = game.add.audio('heartMusic');
        this.heartMusic.loop = false;

        playbg = game.add.image(0, 0, 'playbg');

        var pause_label = game.add.text(game.width - 100, 460, 'Pause', { font: '24px Arial', fill: '#fff' });
        pause_label.inputEnabled = true;
        pause_label.events.onInputUp.add(function () {
            this.text3.visible = true;
            game.paused = true;
        })

        game.input.onDown.add(this.unpause, self);

        

        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();

    },
    update: function() {
        if(status == 'gameOver' && keyboard.enter.isDown) this.restart();
        if(status != 'running') return;
        


        this.physics.arcade.collide(player, platforms, this.effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        this.physics.arcade.collide(player, stars, this.effect2.bind(this));
        this.checkTouchCeiling(player);
        this.checkGameOver();

        this.updatePlayer();
        this.updatePlatforms();
        this.updateTextsBoard();

        this.createPlatforms();

        
    }, 


    unpause: function(event){
        // Only act if paused
        if(game.paused){
                this.text3.visible = false;
                game.paused = false;
            }
    },
    
    createBounders: function() {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        ceiling = game.add.image(0, 0, 'ceiling');
    },

    createPlayer: function() {
        player = game.add.sprite(200, 50, 'player');
        player.direction = 10;
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.animations.add('left', [0, 1, 2, 3], 8);
        player.animations.add('right', [9, 10, 11, 12], 8);
        player.animations.add('flyleft', [18, 19, 20, 21], 12);
        player.animations.add('flyright', [27, 28, 29, 30], 12);
        player.animations.add('fly', [36, 37, 38, 39], 12);
        player.life = 10;
        player.unbeatableTime = 0;
        player.touchOn = undefined;
    },

    createTextsBoard: function() {
        var style = {fill: '#ffffff', fontSize: '20px'}
        text1 = game.add.text(50, 460, '', style);
        text2 = game.add.text(160, 460, '', style);
        text3 = game.add.text(80, 200, 'click any where to resume', style);
        text3.visible = false;
    },

    createPlatforms: function() {
        if(game.time.now > lastTime + 600) {
            lastTime = game.time.now;
            this.createOnePlatform();
            distance += 1;
        }
    },

    createOnePlatform: function(){
        var platform;
        var x = Math.random()*(400 - 96 - 40) + 20;
        var y = 400;
        var rand = Math.random() * 100;
        var rand2 = Math.random() * 100;

        if(rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }
        if(rand2 < 15){
            var y2 = (x%2)?370:355;
            var x2 = (x + 120)%350;
            var x3 = (x2<15)?x2+15:x2;
			var star = game.add.sprite(x3 , y2, 'star');
			game.physics.arcade.enable(star);
			star.body.immovable = true;
			stars.push(star);
        }
        else if(rand2 < 20){
            var y2 = (x%2)?370:355;
            var x2 = (x + 120)%350;
            var x3 = (x2<15)?x2+15:x2;
			var star = game.add.sprite(x3 , y2, 'heart');
			game.physics.arcade.enable(star);
			star.body.immovable = true;
			stars.push(star);
		}

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    updatePlayer: function() {
        if(keyboard.left.isDown) {
                player.body.velocity.x = -250;
            } else if(keyboard.right.isDown) {
                player.body.velocity.x = 250;
            } else {
                player.body.velocity.x = 0;
            }
            this.setPlayerAnimate(player);
        },

    setPlayerAnimate: function() {
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;

        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
        player.frame = 8;
        }
    },

    updatePlatforms: function() {
        for(var i=0; i<platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= 2;
            if(platform.body.position.y <= -18) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
        for(var i=0; i<stars.length; i++) {
			if(stars[i].body)stars[i].body.y -= 2;

			if(stars[i].body.y < 0) {
				stars[i].destroy();
				stars.splice(i, 1);
			}
		}
		
    },

    updateTextsBoard: function() {
        text1.setText('life: ' + player.life);
        text2.setText('score: ' + distance);
    },

    effect: function(player, platform) {
        if(platform.key == 'conveyorRight') {
            player.body.x += 2;
        }
        if(platform.key == 'conveyorLeft') {
            player.body.x -= 2;
        }
        if(platform.key == 'trampoline') {
            this.bounceMusic = game.add.audio('bounceMusic');
            this.bounceMusic.play();
            platform.animations.play('jump');
            player.body.velocity.y = -350;
        }
        if(platform.key == 'nails') {
            if (player.touchOn !== platform) {
                this.nailMusic = game.add.audio('nailMusic');
                this.nailMusic.play();
                player.life -= 1;
                player.touchOn = platform;
                game.camera.flash(0xff0000, 100);
            }
        }
        if(platform.key == 'normal') {
            if (player.touchOn !== platform) {
                player.touchOn = platform;
            }
        }
        if(platform.key == 'fake') {
            if(player.touchOn !== platform) {
                this.fakeMusic = game.add.audio('fakeMusic');
                this.fakeMusic.play();
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }
    }, 

    effect2: function(player, star) {
		for(var i=0; i<stars.length; i++) {
			if(stars[i] === star) {
				if(star.key == 'star'){
                    this.starMusic.play();
					distance += 5;
                }
                else if(star.key == 'heart'){
                    this.heartMusic.play();
                    if(player.life<15)player.life += 1;
                }
				stars[i].destroy();
				stars.splice(i, 1);
			}
		}
	},
    
    checkTouchCeiling: function(player) {
        if(player.body.y < 0) {
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
            if(game.time.now > player.unbeatableTime) {
                player.life -= 1;
                this.nailMusic.play();
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },
    
    checkGameOver: function() {
        if(player.life <= 0 || player.body.y > 420) {
            this.gameOver();
        }
    },
    
    gameOver: function() {
        platforms.forEach(function(s) {s.destroy()});
        platforms = [];
        this.bgmMusic.stop();
        this.ggMusic.play();
        status = 'gameOver';
        game.state.start('input');
    },
    
    restart: function() {
        text3.visible = false;
        distance = 0;
        this.createPlayer();
        this.bgmMusic.play();
        status = 'running';
    }
};