var boardState = {
    preload: function () {
    // Add a 'loading...' label on the screen

    game.load.image('m', 'asset/m.png');
    game.load.image('boardbg', 'asset/boardbg2.jpg');
    game.load.audio('buttonMusic', 'asset/button.wav');
    game.load.audio('menubgmMusic', 'asset/menubgm.ogg');
    

    },

    // Load all game assets
    
    create: function () {
        boardbg = game.add.sprite(0, 0, 'boardbg');
        keyboard = game.input.keyboard.addKeys({
            'm': Phaser.Keyboard.M
        });
        this.buttonMusic = game.add.audio('buttonMusic');
        this.buttonMusic.loop = false;
        this.menubgmMusic = game.add.audio('menubgmMusic');
        this.menubgmMusic.loop = true;
        this.menubgmMusic.play();

    var inputLabel = game.add.text(-50, 60,
    'Leader board', { font: '32px Arial', fill: '#ffffff' });
    inputLabel.anchor.setTo(0.5, 0.5);
    game.add.tween(inputLabel).to({x: 140},1000).easing(Phaser.Easing.Bounce.Out).start();

    mkey = game.add.sprite(330, game.height-90, 'm');
    var menuLabel = game.add.text(350, game.height-30,
        'menu', { font: '22px Arial', fill: '#ffffff' });
    menuLabel.anchor.setTo(0.5, 0.5);

    var record_name = [];
    var score = [];
    var amount = 0;

    var recordRef = firebase.database().ref('record');
    recordRef.orderByChild('score').limitToLast(10).once('value')
            .then(function (snapshot) {
                snapshot.forEach(function(childsnapshot) {
                    var childdata = childsnapshot.val();
                    record_name[record_name.length] = childdata.name;
                    score[score.length] = childdata.score;
                    console.log(childdata.score);
                });
                record_name.reverse();
                score.reverse();
                amount = record_name.length;
                for(j=0;j<=amount;j++){
                    var nameLabel = game.add.text(160, 120 + j*35,
                        record_name[j], { font: '22px Arial', fill: '#ffffff' });
                    nameLabel.anchor.setTo(0.5, 0.5);
                    var scoreLabel = game.add.text(270, 120 + j*35,
                        score[j], { font: '22px Arial', fill: '#ffffff' });
                    scoreLabel.anchor.setTo(0.5, 0.5);
                }
            }).catch(e => console.log(e.message));


        for(var i = 0; i<10; i++){
            var nextLabel = game.add.text(60, 120 + i*35,
                i+1, { font: '22px Arial', fill: '#ffffff' });
            nextLabel.anchor.setTo(0.5, 0.5);
            
        }
    },

    update: function() {
        if(keyboard.m.isDown) this.menu();
    },

    menu: function(){
        this.buttonMusic.play();
        this.menubgmMusic.stop();
        game.state.start('menu');
    }
}; 